<?php

namespace App\Core\UseCase;

use App\Core\Model\Task;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class ICreateTaskTest extends TestCase
{

    private ICreateTask $createTask;

    private ICreateTaskCommandInterface $command;

    private TaskWriteRepositoryInterface $writeRepository;

    public function dataProvider(): array
    {
        return [
            [
                [
                    'title' => 'Tache 1',
                    'description' => 'description tache 1',
                    'creationDate' => new \DateTime('now')
                ]
            ]
        ];
    }

    protected function setUp(): void
    {
        $this->command = m::mock(ICreateTaskCommandInterface::class);
        $this->writeRepository = m::mock(TaskWriteRepositoryInterface::class);
        $this->task = m::mock(Task::class);

        $this->createTask = new ICreateTask($this->command, $this->writeRepository);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testExecute(array $dataProvider)
    {
        $this->command->shouldReceive("execute")->andReturn($this->task);
        $this->writeRepository->shouldReceive("write");
        $this->assertNull($this->createTask->execute($dataProvider));
    }

    public function testExecuteError()
    {
        $this->expectError();
        $this->createTask->execute();
    }
}
