<?php

namespace App\Adapter\LeftAdapter\Controller;

use App\Adapter\LeftAdapter\FormType\TaskType;
use App\Core\UseCase\ICreateTask;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CreateTaskController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    #[Route("/", name: "create_task")]
    public function index(
        Request $request,
        ICreateTask $createTask
    ): Response
    {
        $form = $this->createForm(TaskType::class);
        $form->handleRequest($request);
        $errorMessage = null;
        if($form->isSubmitted()){
            try{
                $createTask->execute($form->getData());
            } catch(\Exception $e){
                $errorMessage = $e->getMessage();
            }
        }

        return $this->render(
            "create_task/index.html.twig",
            [
                "form" => $form->createView(),
                "error_message" => $errorMessage
            ]
        );
    }
}