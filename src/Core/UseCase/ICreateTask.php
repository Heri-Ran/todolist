<?php

namespace App\Core\UseCase;

class ICreateTask
{
    public function __construct(
        ICreateTaskCommandInterface $command,
        TaskWriteRepositoryInterface $writeRepository
    )
    {
        $this->command = $command;
        $this->writeRepository = $writeRepository;
    }

    public function execute(?array $formData): void
    {
        $this->writeRepository->write($this->command->execute($formData));
    }
}