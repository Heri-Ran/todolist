<?php

namespace App\Adapter\RightAdapter;

use App\Core\Model\Task;
use Doctrine\ORM\EntityManagerInterface;

class TaskWriteRepository implements \App\Core\UseCase\TaskWriteRepositoryInterface
{

    public function __construct(
        EntityManagerInterface $entityManager,
        \App\Adapter\RightAdapter\Entity\Task $task
    )
    {
        $this->entityManager = $entityManager;
        $this->task = $task;
    }

    public function write(Task $task): void
    {
        $this->task
            ->setTitle($task->getTitle())
            ->setDescription($task->getDescription())
            ->setCreationDate($task->getCreationDate())
        ;

        $this->entityManager->persist($this->task);
        $this->entityManager->flush();
    }
}