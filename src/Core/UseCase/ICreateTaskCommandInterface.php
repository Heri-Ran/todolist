<?php

namespace App\Core\UseCase;

interface ICreateTaskCommandInterface
{
    public function execute(array $informationForms): \App\Core\Model\Task;
}