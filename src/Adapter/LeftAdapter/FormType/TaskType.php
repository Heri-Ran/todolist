<?php

namespace App\Adapter\LeftAdapter\FormType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class TaskType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                "title",
                TextType::class,
                [
                    "required" => "required"
                ]
            )
            ->add(
                "description",
                TextareaType::class,
                [
                    "required" => false
                ]
            )
            ->add("create", SubmitType::class);
    }
}