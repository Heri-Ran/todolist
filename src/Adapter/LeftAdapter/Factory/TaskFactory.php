<?php

namespace App\Adapter\LeftAdapter\Factory;

use App\Core\Model\Task;

class TaskFactory
{
    public function createTask(): Task
    {
        return new Task();
    }
}