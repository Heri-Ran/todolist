<?php

namespace App\Adapter\LeftAdapter\Factory;

class ReflectionMethodFactory
{
    public function create(object $object, string $method): \ReflectionMethod
    {
        return new \ReflectionMethod($object, $method);
    }
}