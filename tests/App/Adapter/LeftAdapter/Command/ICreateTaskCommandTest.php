<?php

namespace App\Adapter\LeftAdapter\Command;

use App\Adapter\LeftAdapter\Factory\TaskFactory;
use App\Core\Model\Task;
use PHPUnit\Framework\TestCase;

class ICreateTaskCommandTest extends TestCase
{

    private ICreateTaskCommand $createTaskCommand;

    protected function setUp(): void
    {
        $this->taskFactory = \Mockery::mock(TaskFactory::class);
        $this->task = \Mockery::mock(Task::class);
        $this->createTaskCommand = new ICreateTaskCommand($this->taskFactory);
    }

    public function testExecute()
    {
        $this->taskFactory->shouldReceive("createTask")->andReturn($this->task);
        $this->assertInstanceOf(
            Task::class,
            $this->createTaskCommand->execute(
                [
                    "title" => "nqmkjdshkjf",
                    "description" => "lhqdsjfhsqd"
                ]
            ));
    }
}
