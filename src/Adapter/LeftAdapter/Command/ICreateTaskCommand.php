<?php

namespace App\Adapter\LeftAdapter\Command;

use App\Adapter\LeftAdapter\Factory\ReflectionMethodFactory;
use App\Adapter\LeftAdapter\Factory\TaskFactory;

class ICreateTaskCommand implements \App\Core\UseCase\ICreateTaskCommandInterface
{
    public function __construct(
        TaskFactory $taskFactory,
        ReflectionMethodFactory $reflectionMethodFactory
    )
    {
        $this->taskFactory = $taskFactory;
        $this->reflectionMethodFactory = $reflectionMethodFactory;
    }

    public function execute(array $informationForms): \App\Core\Model\Task
    {
        $task = $this->taskFactory->createTask();
        foreach($informationForms as $key => $value)
        {
            $setter = $this->reflectionMethodFactory->create($task, "set".ucfirst($key));
            $setter->invoke($task, $value);
        }

        return $task;
    }
}