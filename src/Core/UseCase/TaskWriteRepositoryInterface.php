<?php

namespace App\Core\UseCase;

use App\Core\Model\Task;

interface TaskWriteRepositoryInterface
{
    public function write(Task $task): void;
}